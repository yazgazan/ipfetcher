
program = require "commander"
get = require "get"

program.version "0.1"
program.option "-S, --server <server address>", "server address (mandatory)"
program.option "-j, --json", "json format (default)"
program.option "-t, --text", "plain text format"
program.option "-i, --ip-only", "print ip only"
program.option "-n, --name <device name>", "device name (mandatory)"
program.option "-g, --get", "'get' action (default)"
program.option "-s, --set", "'set' action"
program.parse process.argv

program.json = program.json or false
program.text = program.text or false
program.ipOnly = program.ipOnly or false
program.get = program.get or false
program.set = program.set or false


server = program.server
name = program.name

if not server? or not name?
  program.help()
  process.exit 1

if (name.indexOf "/") isnt -1
  console.error "device name cannot contain a '/'"
  process.exit 1

if program.text is false
  program.json = true

if program.set is false
  program.get = true

if not server.match /^https?:\/\//gi
  server = "http://#{server}"

if not server.match /\/$/gi
  server = "#{server}/"

action = if program.set is true then "set" else "get"
format = if program.text is true then "text" else "json"
ipOnly = program.ipOnly

url = "#{server}#{name}/#{action}"

req = get
  uri: "#{url}"

req.asBuffer (err, data) ->
  if err and err.status isnt 404
    if format is "json" and not ipOnly
      console.log err
      process.exit 1
      return
    if format is "text" and not ipOnly
      console.log err.code
      process.exit 1
      return
    if ipOnly
      if format is "json"
        process.log new Object
      process.exit 1
      return
  try
    data = JSON.parse data
  catch e
    if format is "json" and not ipOnly
      console.log
        status: "err"
        err: "JSON parse failed"
        detail: e
      process.exit 1
      return
    if format is "text" and not ipOnly
      console.log "JSON parse failed"
      process.exit 1
      return
    if ipOnly
      if format is "json"
        process.log new Object
      process.exit 1
      return
  if format is "json" and not ipOnly
    console.log data
    process.exit 0
    return
  if format is "json" and ipOnly
    if data.ip
      console.log
        ip: data.ip
    else
      console.log new Object
  if format is "text" and not ipOnly
    console.log "#{id}: #{value}" for id, value of data
    process.exit 0
    return
  if format is "text" and ipOnly
    if data.ip
      console.log data.ip
    process.exit 0
    return

