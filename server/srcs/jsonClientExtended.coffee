
KvstoreJSONClient = require "./jsonClient"

class KvstoreJSONClientExt extends KvstoreJSONClient
  constructor: (port, addr) ->
    super port, addr
    @queries = new Array
    @first = true

  repHandler: (rep, cb = null) ->

  addQuery: (cmd, cb) ->
    @queries.push
      cmd: cmd
      cb: cb

  execNextQuery: ->
    if @queries.length is 0
      @first = true
      return
    query = @queries.shift()
    @query query.cmd, ((rep) =>
      if query.cb
        query.cb rep
      else if @default
        @default rep
      @execNextQuery()
    ), true

  query: (cmd, cb = null, useSuper = false) ->
    if useSuper is true
      super cmd, cb
      return
    if @first is true
      @first = false
      @addQuery cmd, cb
      @execNextQuery()
    else
      @addQuery cmd, cb

  whoami: (cb = null) ->
    if cb is null
      throw "Don't ask if you don't care ..."
    cmd =
      cmd: "whoami"
      args: []
    @query cmd, (rep) => cb rep.resp

  whereami: (cb = null) ->
    if cb is null
      throw "Don't ask if you don't care ..."
    cmd =
      cmd: "whereami"
      args: []
    @query cmd, (rep) => cb rep.resp

  alias: (aka, cb = null) ->
    cmd =
      cmd: "alias"
      args: aka
    @query cmd, (rep) => cb rep.resp if cb isnt null

  use: (doc, cb = null) ->
    cmd =
      cmd: "use"
      args: doc
    @query cmd, (rep) => cb rep.resp if cb isnt null

  auth: (key, cb = null) ->
    cmd =
      cmd: "auth"
      args: key
    @query cmd, (rep) => cb rep.resp if cb isnt null

  get: (name, cb = null) ->
    if cb is null
      throw "Don't ask if you don't care ..."
    cmd =
      cmd: "get"
      args: name
    @query cmd, (rep) => cb rep.resp

  set: (name, val, cb = null) ->
    cmd =
      cmd: "set"
      args: [name, val]
    @query cmd, (rep) => cb rep.resp if cb isnt null

  write: (cb = null) ->
    cmd =
      cmd: "write"
      args: []
    @query cmd, (rep) => cb rep.resp if cb isnt null

  listDb: (db, cb = null) ->
    if cb is null
      throw "Don't ask if you don't care ..."
    cmd =
      cmd: "list"
      args: db
    @query cmd, (rep) =>
      if rep.state is "err"
        cb null
      else if (typeof rep.resp) isnt "object"
        cb [rep.resp]
      else
        cb rep.resp

  list: (db = null, cb = null) ->
    if cb isnt null
      @listDb db, cb
      return
    cb = db
    if cb is null
      throw "Don't ask if you don't care ..."
    cmd =
      cmd: "list"
      args: []
    @query cmd, (rep) =>
      if rep.state is "err"
        cb null
      else if (typeof rep.resp) isnt "object"
        cb [rep.resp]
      else
        cb rep.resp

  hold: (cb = null) ->
    cmd =
      cmd: "hold"
      args: []
    @query cmd, (rep) => cb rep.resp if cb isnt null

  abort: (cb = null) ->
    cmd =
      cmd: "abort"
      args: []
    @query cmd, (rep) => cb rep.resp if cb isnt null

  commit: (cb = null) ->
    cmd =
      cmd: "commit"
      args: []
    @query cmd, (rep) => cb rep.resp if cb isnt null

  create: (name, cb = null) ->
    cmd =
      cmd: "create"
      args: name
    @query cmd, (rep) => cb rep.resp if cb isnt null

  delete: (key, cb = null) ->
    cmd =
      cmd: "delete"
      args: key
    @query cmd, (rep) => cb rep.resp if cb isnt null

  drop: (name, cb = null) ->
    cmd =
      cmd: "drop"
      args: name
    @query cmd, (rep) => cb rep.resp if cb isnt null


module.exports = KvstoreJSONClientExt

