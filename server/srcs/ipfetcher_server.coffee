
express = require "express"
fs = require "fs"
Push = require "pushover-notifications"
KvsClient = require "./KvsClient"
app = express()

port = +process.env["PORT"] || 4242
dbport = +process.env["DBPORT"] || 23456
dbauth = process.env["DBAUTH"] || null
pushtoken = process.env["PUSH_TOKEN"] || null
pushkey = process.env["PUSH_KEY"] || null

db = new KvsClient dbport, 'localhost', dbauth

if pushtoken isnt null and pushkey isnt null
  push = new Push
    token: pushtoken
    user: pushkey
else
  push = null

app.get "/:name/set", (req, res) ->
  proxyIp = req.headers['x-real-ip']
  name = req.params.name
  if proxyIp
    ip = proxyIp
  else
    ip = req.connection.remoteAddress
  db.set name, ip, (rep) ->
    console.log "set ok for #{ip} (#{name})"
    res.send 200,
      status: "ok"
      name: "#{name}"
      ip: "#{ip}"
    if push isnt null
      msg =
        message: "#{name} updated ip (#{ip})"
        title: "new ip for #{name}"
      push.send msg

app.get "/:name/get", (req, res) ->
  name = req.params.name
  db.get name, (ip) ->
    if ip is null
      res.send 200,
        status: "err"
        err: "device not found"
        name: "#{name}"
        ip: "#{null}"
    else
      console.log "get ok for #{ip} (#{name})"
      res.send 200,
        status: "ok"
        name: "#{name}"
        ip: "#{ip}"

app.listen port

console.error "listening on #{port}"

