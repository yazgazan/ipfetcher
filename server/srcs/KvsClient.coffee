
KvstoreJSONClient = require "./jsonClient"
KvstoreJSONClientExt = require "./jsonClientExtended"

class KvsClient extends KvstoreJSONClientExt
  constructor: (port, addr, @authkey = null) ->
    super port, addr

  connect: ->
    @alias 'ipfetcher'
    if @authkey isnt null
      @auth @authkey
    @create "ipfetcher"
    @use "ipfetcher"

  on: (event, cb) ->
    @sock.on event, cb

class KvsClientManager
  constructor: (@port, @addr, @auth = null) ->
    @initClient()

  initClient: ->
    @client = new KvsClient @port, @addr, @auth
    @client.on 'error', (e) => @clientError e
    @client.on 'close', () => @clientClose()

  clientError: (e) ->
    console.log "KvsClient got an error (", e, "), restarting client"
    @initClient()

  clientClose: ->
    console.log "KvsClient closed, restarting client"
    @initClient()

  get: (name, cb) -> @client.get name, cb

  set: (name, value, cb) -> @client.set name, value, cb

module.exports = KvsClientManager

