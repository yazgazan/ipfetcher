
net = require 'net'

class KvstoreJSONClient
  constructor: (@port, @address = "localhost") ->
    @lastId = 0
    @cbs = new Object
    @sock = net.createConnection @port, @address
    @sock.setEncoding 'ascii'
    @sock.on 'data', (chunk) => @handleRep chunk
    @sock.on 'connect', => @connect()

  split: (chunk) ->
    ret = new Array
    chunk = chunk.split "\r\n"
    for line in chunk
      tmp = line.split "\n"
      for line2 in tmp
        continue if line2.length is 0
        ret.push line2
    return ret

  handleRep: (chunk) ->
    reps = @split chunk
    for rep in reps
      try
        rep = JSON.parse rep
      catch e
        continue
      if not @cbs[rep.id]?
        @default rep if @default
      else
        @cbs[rep.id] rep
        delete @cbs[rep.id]
    return

  query: (cmd, cb = null) ->
    cmd.id = ++@lastId
    @cbs[cmd.id] = cb if cb
    @sock.write "#{JSON.stringify cmd}\n"


module.exports = KvstoreJSONClient

